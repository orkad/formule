﻿using System;
using System.Globalization;

namespace ClassLibrary
{
	public class Formule
	{
		protected bool Equals(Formule other)
		{
			return Resultat.Equals(other.Resultat);
		}

		public override bool Equals(object obj)
		{
			if (ReferenceEquals(null, obj)) return false;
			if (ReferenceEquals(this, obj)) return true;
			if (obj.GetType() != GetType()) return false;
			return Equals((Formule) obj);
		}

		public override int GetHashCode()
		{
			return Resultat.GetHashCode();
		}

		#region CONST

		public const int PrioAdd = 1;
		public const int PrioSou = 2;
		public const int PrioMul = 3;
		public const int PrioDiv = 4;
		public const int PrioVar = 5;

		#endregion

		#region PROPRIETE

		/// <summary>
		/// Evaluation litterale de la formule
		/// </summary>
		public string Litterale { get; private set; }

		/// <summary>
		/// Evaluation litterale de la formule entre parenthèse
		/// </summary>
		public string LitteraleParenthese
		{
			get { return "(" + Litterale + ")"; }
		}

		/// <summary>
		/// Evaluation numérique de la formule
		/// </summary>
		public string Numerique { get; private set; }

		/// <summary>
		/// Evaluation numérique de la formule entre parenthèse
		/// </summary>
		public string NumeriqueParenthese
		{
			get { return "(" + Numerique + ")"; }
		}

		/// <summary>
		/// Résultat de la formule
		/// </summary>
		public double Resultat { get; private set; }

		/// <summary>
		/// Priorité de la dernière opération
		/// utilisé pour savoir si des parenthèses doivent être mise durant les opérations mathématiques
		/// </summary>
		private int Priorite{ get; set; }

		#endregion

		#region CONSTRUCTEURS

		public Formule(string litterale, string numerique, double resultat, int priorite)
		{
			Litterale = litterale;
			Numerique = numerique;
			Resultat = resultat;
			Priorite = priorite;
		}

		public static implicit operator Formule(Variable variable)
		{
			return new Formule(variable.Nom, variable.Valeur.ToString(CultureInfo.InvariantCulture), variable.Valeur, PrioVar);
		}

		#endregion

		#region OPERATEURS MATHEMATIQUE FORMULE / FORMULE

		public static Formule operator +(Formule f1, Formule f2)
		{
			//Il n'y aura jamais besoin de parenthese pour une addition entre formule, l'addition étant l'opérateur le moins prioritaire
			string lit = f1.Litterale + "+" + f2.Litterale;
			string num = f1.Numerique + "+" + f2.Numerique;
			double res = f1.Resultat + f2.Resultat;
			return new Formule(lit,num,res,PrioAdd);
		}

		public static Formule operator -(Formule f1, Formule f2)
		{
			//Il n'y aura jamais besoin de parenthese pour une formule de l'operande de gauche pour une soustraction
			string lit = f1.Litterale;
			string num = f1.Numerique;
			//S'il y a une soustraction ou une addition dans l'operande de gauche la formule de droite doit être entre parenthese
			//Pour une multiplication ou une division pas besoin de parenthese car elles sont priotaire
			lit += "-";
			num += "-";
			if (f2.Priorite <= PrioSou)
			{
				lit += f2.LitteraleParenthese;
				num += f2.NumeriqueParenthese;
			}
			else
			{
				lit += f2.Litterale;
				num += f2.Numerique;
			}
			double res = f1.Resultat - f2.Resultat;
			return new Formule(lit, num, res, PrioSou);
		}

		public static Formule operator *(Formule f1, Formule f2)
		{
			string lit;
			string num;
			//Si une soustraction ou une addition a été effectué dans l'operande de gauche elle doit alors être mis entre parenthese
			if (f1.Priorite < PrioMul)
			{
				lit = f1.LitteraleParenthese;
				num = f1.NumeriqueParenthese;
			}
			else
			{
				lit = f1.Litterale;
				num = f1.Numerique;
			}
			lit += "*";
			num += "*";
			//Si une soustraction ou une addition a été effectué dans l'operande de droite elle doit alors être mis entre parenthese
			if (f2.Priorite < PrioMul)
			{
				lit += f2.LitteraleParenthese;
				num += f2.NumeriqueParenthese;
			}
			else
			{
				lit += f2.Litterale;
				num += f2.Numerique;
			}
			double res = f1.Resultat * f2.Resultat;
			return new Formule(lit, num, res, PrioMul);
		}

		/// <summary>
		/// Cas d'une division d'une formule par une autre formule
		/// </summary>
		/// <param name="f1">operande de gauche</param>
		/// <param name="f2">operande de droite</param>
		/// <returns></returns>
		public static Formule operator /(Formule f1, Formule f2)
		{
			string lit;
			string num;
			//Si la priorité est inférieure à celle d'une multiplication pour l'operande de gauche elle doit avoir des parenthèses
			if (f1.Priorite < PrioMul)
			{
				lit = f1.LitteraleParenthese;
				num = f1.NumeriqueParenthese;
			}
			else
			{
				lit = f1.Litterale;
				num = f1.Numerique;
			}
			lit += "/";
			num += "/";
			//Meme s'il y a une division dans la formule de l'operande de droite elle doit être prioritaire car V / V / V != V / (V / V)
			if (f2.Priorite <= PrioDiv)
			{
				lit += f2.LitteraleParenthese;
				num += f2.NumeriqueParenthese;
			}
			else
			{
				lit += f2.Litterale;
				num += f2.Numerique;
			}
			double res = f1.Resultat / f2.Resultat;
			return new Formule(lit, num, res, PrioDiv);
		}

		#endregion

		#region OPERATEURS MATHEMATIQUE VARIABLE / FORMULE

		public static Formule operator +(Variable v, Formule f)
		{

			string lit;
			string num;
			if (f.Priorite == PrioSou)
			{
				lit = v.Nom + "+" + f.LitteraleParenthese;
				num = v.Valeur + "+" + f.NumeriqueParenthese;
			}
			else
			{
				lit = v.Nom + "+" + f.Litterale;
				num = v.Valeur + "+" + f.Numerique;
			}
			double res = v.Valeur + f.Resultat;
			return new Formule(lit, num, res, PrioAdd);
		}

		public static Formule operator -(Variable v, Formule f)
		{
			string lit;
			string num;
			if (f.Priorite <= PrioSou)
			{
				lit = v.Nom + "-" + f.LitteraleParenthese;
				num = v.Valeur + "-" + f.NumeriqueParenthese;
			}
			else
			{
				lit = v.Nom + "-" + f.Litterale;
				num = v.Valeur + "-" + f.Numerique;
			}
			double res = v.Valeur - f.Resultat;
			return new Formule(lit, num, res, PrioSou);
		}

		public static Formule operator *(Variable v, Formule f)
		{
			string lit;
			string num;
			if(f.Priorite != PrioMul)
			{
				lit = v.Nom + "*" + f.LitteraleParenthese;
				num = v.Valeur + "*" + f.NumeriqueParenthese;
			}
			else
			{
				lit = v.Nom + "*" + f.Litterale;
				num = v.Valeur + "*" + f.Numerique;
			}
			double res = v.Valeur * f.Resultat;
			return new Formule(lit, num, res, PrioMul);
		}

		public static Formule operator /(Variable v, Formule f)
		{
			var lit = v.Nom + "/" + f.LitteraleParenthese;
			var num = v.Valeur + "/" + f.NumeriqueParenthese;
			double res = v.Valeur / f.Resultat;
			return new Formule(lit, num, res, PrioDiv);
		}

		#endregion

		#region OPERATEURS MATHEMATIQUE FORMULE / VARIABLE

		public static Formule operator +(Formule f, Variable v)
		{
			var lit = f.Litterale + "+" + v.Nom;
			var num = f.Numerique + "+" + v.Valeur;
			double res = f.Resultat + v.Valeur;
			return new Formule(lit, num, res, PrioAdd);
		}

		public static Formule operator -(Formule f, Variable v)
		{
			var lit = f.Litterale + "-" + v.Nom;
			var num = f.Numerique + "-" + v.Valeur;

			double res = f.Resultat - v.Valeur;
			return new Formule(lit, num, res, PrioSou);
		}

		public static Formule operator *(Formule f, Variable v)
		{
			string lit;
			string num;
			if (f.Priorite < PrioMul)
			{
				lit = f.LitteraleParenthese + "*" + v.Nom;
				num = f.NumeriqueParenthese + "*" + v.Valeur;
			}
			else
			{
				lit = f.Litterale + "*" + v.Nom;
				num = f.Numerique + "*" + v.Valeur;
			}
			double res = f.Resultat * v.Valeur;
			return new Formule(lit, num, res, PrioMul);
		}

		public static Formule operator /(Formule f, Variable v)
		{
			string lit;
			string num;
			if(f.Priorite < PrioMul)
			{
				lit = f.LitteraleParenthese + "/" + v.Nom;
				num = f.NumeriqueParenthese + "/" + v.Valeur;
			}
			else
			{
				lit = f.Litterale + "/" + v.Nom;
				num = f.Numerique + "/" + v.Valeur;
			}
			double res = f.Resultat / v.Valeur;
			return new Formule(lit, num, res, PrioDiv);
		}

		#endregion

		#region OPERATEURS DE COMPARAISON FORMULE / FORMULE

		public static bool operator ==(Formule f1, Formule f2)
		{
			return f1 != null && f1.Equals(f2);
		}

		public static bool operator !=(Formule f1, Formule f2)
		{
			return !(f1 == f2);
		}

		#endregion

		#region METHODES

		public Variable ToVariable(string nom)
		{
			return new Variable(nom, Resultat);
		}

		public void ToConsole()
		{
			Console.WriteLine("\n------------------------------   CALCUL   --------------------------------------");
			Console.WriteLine("Evaluation Litterale : " + Litterale);
			Console.WriteLine("Evaluation Numerique : " + Numerique);
			Console.WriteLine("Resultat :" + Resultat);
		}

		public void ConsoleUnitTest(string exLitterale)
		{
			Console.WriteLine("\n-------------------------------   TEST   ---------------------------------------");
			if (Litterale == exLitterale)
			{
				Console.WriteLine("Test Evaluation Litterale : Ok ");
				Console.WriteLine("Formule : " + Litterale);
			}
			else
			{
				Console.WriteLine("Test Evaluation Litterale : Echec ");
				Console.WriteLine("Calculée : " + Litterale);
				Console.WriteLine("Attendue : " + exLitterale);
			}
				
		}

		public void ConsoleUnitTest(string exLitterale, string exNumerique)
		{
			ConsoleUnitTest(exLitterale);
			Console.WriteLine(Numerique == exNumerique ? "Test Evaluation Numerique : Ok" : "Test Evaluation Numerique : Echec");
		}

		public void ConsoleUnitTest(string exLitterale, string exNumerique, double exResultat)
		{
			ConsoleUnitTest(exLitterale,exNumerique);
			Console.WriteLine(exResultat == Resultat ? "Test Resultat : Ok" : "Test Resultat : Echec");
		}

		#endregion

		#region FONCTIONS MATHEMATIQUES

		/// <summary>
		/// Fonction de puissance pour une variable
		/// </summary>
		/// <param name="v"></param>
		/// <param name="puissance"></param>
		/// <returns></returns>
		public static Formule Pow(Variable v, double puissance)
		{
			return new Formule(v.Nom + "^" + puissance, v.Valeur + "^" + puissance, Math.Pow(v.Valeur, puissance), PrioVar);
		}

		/// <summary>
		/// Logarithme népérien pour une formule
		/// </summary>
		/// <param name="f">Formule concernée</param>
		/// <returns></returns>
		public static Formule Log(Formule f)
		{
			if (f.Resultat <= 0)
				throw new ArithmeticException("Log négatif");
			return new Formule("log" + f.LitteraleParenthese, "log" + f.NumeriqueParenthese, Math.Log(f.Resultat), PrioVar);
		}

		/// <summary>
		/// Fonction Racine carre pour une formule
		/// </summary>
		/// <param name="f">formule concernée</param>
		/// <returns></returns>
		public static Formule Sqrt(Formule f)
		{
			return new Formule("sqrt" + f.LitteraleParenthese, "sqrt" + f.NumeriqueParenthese, Math.Sqrt(f.Resultat), PrioVar);
		}

		/// <summary>
		/// Fonction Racine carre pour une Variable
		/// </summary>
		/// <param name="v">variable concernée</param>
		/// <returns></returns>
		public static Formule Sqrt(Variable v)
		{
			return new Formule("sqrt(" + v.Nom + ")", "sqrt(" + v.Valeur + ")", Math.Sqrt(v.Valeur), PrioVar);
		}


		/// <summary>
		/// Fonction Cosinus pour un angle en degrès
		/// </summary>
		/// <param name="deg">Variable de l'angle en degrès</param>
		/// <returns></returns>
		public static Formule CosDeg(Variable deg)
		{
			return new Formule("cos(" + deg.Nom + ")", "cos(" + deg.Valeur + ")", Math.Cos(DegToRad(deg.Valeur)), PrioVar);
		}

		/// <summary>
		/// Fonction Cosinus pour un angle en radian
		/// </summary>
		/// <param name="rad">Variable de l'angle en radian</param>
		/// <returns></returns>
		public static Formule CosRad(Variable rad)
		{
			return new Formule("cos(" + rad.Nom + ")", "cos(" + RadToDeg(rad.Valeur) + ")", Math.Cos(rad.Valeur), PrioVar);
		}

		/// <summary>
		/// Fonction Sinus pour un angle en degrès
		/// </summary>
		/// <param name="deg">Variable de l'angle en degrès</param>
		/// <returns></returns>
		public static Formule SinDeg(Variable deg)
		{
			return new Formule("sin(" + deg.Nom + ")", "sin(" + deg.Valeur + ")", Math.Sin(DegToRad(deg.Valeur)), PrioVar);
		}

		/// <summary>
		/// Fonction Sinus pour un angle en radian
		/// </summary>
		/// <param name="rad">Variable de l'angle en radian</param>
		/// <returns></returns>
		public static Formule SinRad(Variable rad)
		{
			return new Formule("sin(" + rad.Nom + ")", "sin(" + RadToDeg(rad.Valeur) + ")", Math.Sin(rad.Valeur), PrioVar);
		}

		/// <summary>
		/// Conversion degrès en radian
		/// </summary>
		/// <param name="deg">Valeur en degrès</param>
		/// <returns></returns>
		public static double DegToRad(double deg)
		{
			return deg * Math.PI / 180;
		}

		/// <summary>
		/// Conversion radian en degrès
		/// </summary>
		/// <param name="rad">Valeur eb radian</param>
		/// <returns></returns>
		public static double RadToDeg(double rad)
		{
			return rad * 180 / Math.PI;
		}

		#endregion
	}
}
