﻿using System;
using System.Globalization;

namespace ClassLibrary
{
	public class Variable
	{
		#region PROPRIETES

		/// <summary>
		/// Nom de la variable
		/// </summary>
		public string Nom { get; private set; }

		/// <summary>
		/// Valeur de la variable
		/// </summary>
		public double Valeur { get; set; }

		#endregion

		#region CONSTRUCTEUR

		/// <summary>
		/// Création d'une variable 
		/// </summary>
		/// <param name="nom">nom de la variable</param>
		/// <param name="valeur">valeur de la variable</param>
		public Variable(string nom, double valeur)
		{
			Nom = nom;
			Valeur = valeur;
		}

		/// <summary>
		/// Création d'une constante (nom = valeur) correspond au cast d'un double vers un type Variable
		/// </summary>
		/// <param name="constante">valeur de la constante</param>
		public Variable(double constante)
		{
			Nom = constante.ToString(CultureInfo.InvariantCulture);
			Valeur = constante;
		}

		#endregion

		public static implicit operator Variable(double d)
		{
			return new Variable(d.ToString(CultureInfo.InvariantCulture), d);
		}

		#region OPERATEUR MATHEMATIQUE VARIABLE / VARIABLE

		public static Formule operator +(Variable v1, Variable v2)
		{
			string lit = v1.Nom + "+" + v2.Nom;
			string num = v1.Valeur + "+" + v2.Valeur;
			double res = v1.Valeur + v2.Valeur;
			return new Formule(lit, num, res, Formule.PrioAdd);
		}

		public static Formule operator -(Variable v1, Variable v2)
		{
			string lit = v1.Nom + "-" + v2.Nom;
			string num = v1.Valeur + "-" + v2.Valeur;
			double res = v1.Valeur - v2.Valeur;
			return new Formule(lit, num, res, Formule.PrioSou);
		}

		public static Formule operator *(Variable v1, Variable v2)
		{
			string lit = v1.Nom + "*" + v2.Nom;
			string num = v1.Valeur + "*" + v2.Valeur;
			double res = v1.Valeur * v2.Valeur;
			return new Formule(lit, num, res, Formule.PrioMul);
		}

		public static Formule operator /(Variable v1, Variable v2)
		{
			if (v2.Valeur == 0)
				throw new DivideByZeroException();
			string lit = v1.Nom + "/" + v2.Nom;
			string num = v1.Valeur + "/" + v2.Valeur;
			double res = v1.Valeur / v2.Valeur;
			return new Formule(lit, num, res, Formule.PrioDiv);
		}

		#endregion
	}
}
