﻿using System;
using ClassLibrary;


class Program
{
	static void Main()
	{
		Variable P = new Variable(Grec.Beta,15);
		Variable E = new Variable("E",0.5);
		Variable R = new Variable("R",200);
		Variable Ro = new Variable("Ro", 250);
		Variable t = new Variable("t",20);

		F_S_A13_2(P, E, R, t).ToConsole();
		F_S_A13_2bis(P, E, Ro, t).ToConsole();

        Variable a = new Variable("a",1);
        Variable b = new Variable("b",2);

	    Formule c = a + b;
        c.ToConsole();
		Console.ReadKey();
	}
	/// <summary>
	/// Calcul de la contrainte maximale admissible pour une enveloppe sphérique a paroi épaisse avec R connu
	/// </summary>
	/// <param name="P">pression intérieure de calcul (voir UG-21)</param>
	/// <param name="E">coefficient de joint relatif au joint considéré dans une enveloppe sphérique</param>
	/// <param name="R">rayon intérieur de la portion d'enveloppe considérée</param>
	/// <param name="t">épaisseur minimale requise pour l'enveloppe</param>
	/// <returns></returns>
	public static Formule F_S_A13_2(Variable P, Variable E, Variable R, Variable t)
	{
		return P / (2 * E * Formule.Log((R + t) / R));
	}

	/// <summary>
	/// Calcul de la contrainte maximale admissible pour une enveloppe sphérique a paroi épaisse avec Ro connu
	/// </summary>
	/// <param name="P">pression intérieure de calcul (voir UG-21)</param>
	/// <param name="E">coefficient de joint relatif au joint considéré dans une enveloppe sphérique</param>
	/// <param name="Ro">rayon extérieur de la portion d'enveloppe considérée</param>
	/// <param name="t">épaisseur minimale requise pour l'enveloppe</param>
	/// <param name="S">valeur de la contrainte minimale admissible, (voir UG-23 et les limites de contraintes spécifiées en UG-24)</param>
	/// <returns></returns>
	public static Formule F_S_A13_2bis(Variable P, Variable E, Variable Ro, Variable t)
	{
		return P / (2 * E * Formule.Log(Ro / (Ro - t)));
	}

}
